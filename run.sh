HOST=ip-172-31-42-182.us-west-2.compute.internal
DOMAIN=ip-172-31-42-182.us-west-2.compute.internal
SUBDOMAIN=jitsi-videobridge01
PORT=5347
SECRET=01
JVB_HOME=`pwd`

mvn compile exec:java -Dexec.args="--host=$DOMAIN --domain=$DOMAIN --subdomain=$SUBDOMAIN --port=$PORT --secret=$SECRET --apis=rest,xmpp" -Djava.library.path=$JVB_HOME/lib/native/linux-64 -Djava.util.logging.config.file=$JVB_HOME/lib/logging.properties -Dnet.java.sip.communicator.SC_HOME_DIR_NAME=.jitsi-videobridge