package org.jitsi.videobridge.stats;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.jitsi.util.Logger;

import java.util.Map;

/**
 * Created by mtcodez on 7/31/16.
 */
public class InfluxDbTransport extends StatsTransport {
    private static final Logger logger = Logger.getLogger(InfluxDbTransport.class);

    private static String HOST;
    private static String USERNAME;
    private static String PASSWORD;
    private static String DATABASE_NAME;

    private final InfluxDB influxdb;
    public InfluxDbTransport(String host, String username, String password, String database) {
        logger.info("InfluxDB initializing!!");
        HOST = host;
        USERNAME = username;
        PASSWORD = password;
        DATABASE_NAME = database;
        this.influxdb = InfluxDBFactory.connect(HOST, USERNAME, PASSWORD);
    }

    @Override
    public void publishStatistics(Statistics statistics) {
        BatchPoints batchPoints = BatchPoints
                .database(DATABASE_NAME)
                .tag("async", "true")
                .tag("jvb", System.getProperty("jvb-name"))
                .retentionPolicy("default")
                .consistency(InfluxDB.ConsistencyLevel.ALL)
                .build();
        for (Map.Entry<String, Object> entry : statistics.getStats().entrySet()) {
            logger.debug(entry);
            Point point = null;
            if (entry.getValue() instanceof String) {
                point = Point.measurement(entry.getKey())
                        .addField("value", (String) entry.getValue())
                        .build();
            } else if (entry.getValue() instanceof Double) {
                point = Point.measurement(entry.getKey())
                        .addField("value", (Double) entry.getValue())
                        .build();
            } else if (entry.getValue() instanceof Float) {
                point = Point.measurement(entry.getKey())
                        .addField("value", (Float) entry.getValue())
                        .build();
            } else if (entry.getValue() instanceof Integer) {
                point = Point.measurement(entry.getKey())
                        .addField("value", (Integer) entry.getValue())
                        .build();
            } else if (entry.getValue() instanceof Boolean) {
                point = Point.measurement(entry.getKey())
                        .addField("value", (Boolean) entry.getValue())
                        .build();
            } else if (entry.getValue() instanceof Long) {
                point = Point.measurement(entry.getKey())
                        .addField("value", (Long) entry.getValue())
                        .build();
            }
            batchPoints.point(point);
        }
        influxdb.write(batchPoints);
    }
}
