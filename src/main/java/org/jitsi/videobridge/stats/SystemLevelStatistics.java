package org.jitsi.videobridge.stats;

import net.gionn.linuxmetrics4j.client.LinuxMetricsClient;
import net.gionn.linuxmetrics4j.client.LinuxMetricsClientImpl;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by mtcodez on 7/31/16.
 */
public class SystemLevelStatistics { //extends Statistics {
    // iostat
    public static final String CPU_USER = "cpu_user";
    public static final String CPU_SYSTEM = "cpu_system";
    public static final String CPU_IOWAIT = "cpu_iowait";
    public static final String CPU_IDLE = "cpu_idle";
//    public static final String CPU_IRQ = "cpu_irq";
//    public static final String DISK_READ_PER_SEC = "disk_read_per_sec";
//    public static final String DISK_WRITE_PER_SEC = "disk_write_per_sec";
//    public static final String DISK_AWAIT = "disk_await";
//    public static final String DISK_UTIL_RATE = "disk_util_rate";
    // vmstat
//    public static final String MEMORY_SWAP_IN = "memory_swap_in";
//    public static final String MEMORY_SWAP_OUT = "memory_swap_out";
    // nicstat
//    public static final String NETWORK_READ_KBS = "network_read_kbs";
//    public static final String NETWORK_WRITE_KBS = "network_write_kbs";
//    public static final String NETWORK_READ_PKTS = "network_read_pkts";
//    public static final String NETWORK_WRITE_PKTS = "network_write_pkts";
//    public static final String NETWORK_UTIL_RATE = "network_util_rate";
//    public static final String NETWORK_SAT = "network_sat";
    // avgload
    public static final String AVG_LOAD_1 = "avgload_1";
    public static final String AVG_LOAD_5 = "avgload_5";
    public static final String AVG_LOAD_15 = "avgload_15";

    double cpuUser = 0;
    double cpuSystem = 0;
    double cpuIoWait = 0;
    double cpuIdle = 0;
//    double cpuIrq = 0;
//    double diskReadPerSec = 0;
//    double diskWritePerSec = 0;
//    double diskAwait = 0;
//    double diskUtilRate = 0;
//    long memorySwapIn = 0;
//    long memorySwapOut = 0;
//    double networkReadKbs = 0;
//    double networkWriteKbs = 0;
//    double networkReadPkts = 0;
//    double networkWritePkts = 0;
//    double networkUtilRate = 0;
//    double networkSat = 0;
    double avgLoad1 = 0;
    double avgLoad5 = 0;
    double avgLoad15 = 0;

    public double getCpuUser() {
        return cpuUser;
    }

    public double getCpuSystem() {
        return cpuSystem;
    }

    public double getCpuIoWait() {
        return cpuIoWait;
    }

    public double getCpuIdle() {
        return cpuIdle;
    }

//    public double getCpuIrq() {
//        return cpuIrq;
//    }

//    public double getDiskReadPerSec() {
//        return diskReadPerSec;
//    }

//    public double getDiskWritePerSec() {
//        return diskWritePerSec;
//    }

//    public double getDiskAwait() {
//        return diskAwait;
//    }

//    public double getDiskUtilRate() {
//        return diskUtilRate;
//    }

//    public long getMemorySwapIn() {
//        return memorySwapIn;
//    }

//    public long getMemorySwapOut() {
//        return memorySwapOut;
//    }

//    public double getNetworkReadKbs() {
//        return networkReadKbs;
//    }
//
//    public double getNetworkWriteKbs() {
//        return networkWriteKbs;
//    }
//
//    public double getNetworkReadPkts() {
//        return networkReadPkts;
//    }
//
//    public double getNetworkWritePkts() {
//        return networkWritePkts;
//    }

//    public double getNetworkUtilRate() {
//        return networkUtilRate;
//    }

//    public double getNetworkSat() {
//        return networkSat;
//    }

    public double getAvgLoad1() {
        return avgLoad1;
    }

    public double getAvgLoad5() {
        return avgLoad5;
    }

    public double getAvgLoad15() {
        return avgLoad15;
    }

    private boolean inGenerate = false;
    private final Map<String,Object> stats = new HashMap<>();

    private final ExecutorService pool = Executors.newFixedThreadPool(4);
    private final LinuxMetricsClient client;

    public SystemLevelStatistics() {
        client = new LinuxMetricsClientImpl();
    }

    public void generate0() {

            try {
                // cpu
//                String[] cpu = getProcessResult(new String[]{"/bin/sh", "-c", "iostat -xm | awk '/^ /{print $1,$3,$4,$5,$6}'"})
//                        .get()
//                        .split("\\s+");
//                cpuUser = Double.valueOf(cpu[0]);
//                cpuSystem = Double.valueOf(cpu[1]);
//                cpuIoWait = Double.valueOf(cpu[2]);
//                cpuIrq = Double.valueOf(cpu[3]);
//                cpuIdle = Double.valueOf(cpu[4])
                cpuUser = client.getCpuUsage().getUser().doubleValue() / 100.0;
                cpuSystem = client.getCpuUsage().getSystem().doubleValue() / 100.0;
                cpuIoWait = client.getCpuUsage().getIowait().doubleValue() / 100.0;
//                cpuIrq = client.getCpuUsage().getIrq().doubleValue() / 100.0;
                cpuIdle = client.getCpuUsage().getIdle().doubleValue() / 100.0;
                avgLoad1 = client.getLoadAverage().getOneMinute();
                avgLoad5 = client.getLoadAverage().getFiveMinutes();
                avgLoad15 = client.getLoadAverage().getFifteenMinutes();

                // disk
//                String[] disk = getProcessResult(new String[]{"/bin/sh", "-c", "iostat -xm | awk '/xvda /{print $6,$7,$10,$14}'"})
//                        .get()
//                        .split("\\s+");
//                diskReadPerSec = Double.valueOf(disk[0]);
//                diskWritePerSec = Double.valueOf(disk[1]);
//                diskAwait = Double.valueOf(disk[2]);
//                diskUtilRate = Double.valueOf(disk[3]);

                // memory
//                String[] memory = getProcessResult(new String[]{"/bin/sh", "-c", "vmstat | awk '/[0-9] /{print $7,$8}'"})
//                        .get()
//                        .split("\\s+");
//                memorySwapIn = Long.valueOf(memory[0]);
//                memorySwapOut = Long.valueOf(memory[1]);

                // network
//                String[] network = getProcessResult(new String[]{"/bin/sh", "-c", "nicstat | awk '/eth0 /{print $3,$4,$5,$6,$9,$10}'"})
//                        .get()
//                        .split("\\s+");
//                networkReadKbs = Double.valueOf(network[0]);
//                networkWriteKbs = Double.valueOf(network[1]);
//                networkReadPkts = Double.valueOf(network[2]);
//                networkWritePkts = Double.valueOf(network[3]);
//                networkUtilRate = Double.valueOf(network[4]);
//                networkSat = Double.valueOf(network[5]);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

    }

    public Future<String> getProcessResult(final String[] command) throws IOException {
        return pool.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                final Process process = Runtime.getRuntime().exec(command);
                String output = null;
                try (InputStream input = process.getInputStream()) {
                    output = IOUtils.toString(input, StandardCharsets.UTF_8);
                    process.waitFor();
                }
                return output;
            }
        });
    }

}
