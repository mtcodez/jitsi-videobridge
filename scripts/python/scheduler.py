import sys
import glob

from scheduling import Scheduler
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Invalid number of arguments. You need two, host and port respectively.'
    transport = TSocket.TSocket(sys.argv[1], int(sys.argv[2]))

    # Buffering is critical. Raw sockets are very slow
    transport = TTransport.TBufferedTransport(transport)

    # Wrap in a protocol
    protocol = TBinaryProtocol.TBinaryProtocol(transport)

    # Create a client to use the protocol encoder
    client = Scheduler.Client(protocol)

    # Connect!
    transport.open()

    print(client.getJvbNumber())