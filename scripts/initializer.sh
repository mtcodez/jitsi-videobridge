#!/bin/bash

changeRunScript() {
    sed -r -i.bak "s/(SUBDOMAIN=jitsi-videobridge)(.*)/\1$1/; s/(SECRET=)(.*)/\1$1/" ../run.sh;
}

if [ "$#" -ne 2 ]; then
    echo "illegal number of parameters";
    exit -1;
fi

echo "===>>> START communicating to JICOFO ======";
sleep 5;

jvbNumber=$(python python/scheduler.py $1 $2);

echo "== Get jvb ${jvbNumber} =="
changeRunScript ${jvbNumber};

sleep 2;
echo "===>>> START jvb ======";
cd ..;tmux new -d '/home/ubuntu/jitsi-videobridge/run.sh';